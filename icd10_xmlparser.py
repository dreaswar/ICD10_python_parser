#! /usr/bin/python

#Import The Requirements
#from elementtree import ElementTree as et
import xml.etree.ElementTree as ET
import sqlite3

import yaml, json

#Parse the XML and get the Tree Element
tree = ET.parse('ICD10CM_2012_Full_Tabular.xml')
root = tree.getroot()
print "Parsed the XML File ..."
print "Root's Tag is: ", root.tag
print "Root's attribute is: ", root.attrib


#Setup DB Connection Object and Cursor
print "Setting up DB connection and Cursor"
db_conn = sqlite3.connect('icd10.db')
db_conn.execute("PRAGMA foreign_keys = ON")
db_cursor = db_conn.cursor()


#Define and Open Files for Reading and Writing
print "Setting up Files for Reading and Writing.."
f          = open('ICD10CM_2012_Full_Tabular.xml','r')
w          = open('chapter.txt','w')
dummy_file = open('dummy_file.txt','w')

#Setup YAML Files to write to and Fixtures for Django
chapter_yaml = file('initial_data.yaml','w')
chapter_yaml_text = file('chapter.yaml','w')
section_yaml = file('section.yaml','w')
diag_yaml    = file('diag.yaml','w')

#Setup JSON Files to write to and Fixtures for Django
initial_data_json = file('initial_icd10_data.json','w')
json_list = []

chapter_list = []
section_list = []
diagnosis_list = []

chapter_json = file('chapter.json','w')
section_json = file('section.json','w')
diag_json    = file('diag.json','w')



SECTION_ID = 1
DIAG_ID  =1

def dump_the_json():
  entire_fixture = json.dumps(json_list, indent=4)
  initial_data_json.write(entire_fixture)
  initial_data_json.close()
  

def _create_diag_table(diag_obj, x):
  global DIAG_ID
  section_table_fk    = x

  for v in diag_obj:
    #section_table_fk    = k
    #diag_elems          = v
    #i=0
    #for inner_elem in diag_elems:
    diag_name    = v.find('name').text
    diag_code    = v.find('desc').text
    print "Adding ", diag_name , " and ", diag_code, "into TABLE", section_table_fk, " now"

    diag_yaml_dict={}
    diag_yaml_dict['fields']={'diag_name'  : diag_name,
                                'diag_code'  : diag_code,
                                'section_fk' : int(x)
                              }
    diag_yaml_dict['model'] = "icd10.diagnosis"
    diag_yaml_dict['pk']    = DIAG_ID
    #yaml.dump(diag_yaml_dict, chapter_yaml)

    diag_json_dict={}
    diag_json_dict['fields']={'diag_name'  : diag_name,
                                'diag_code'  : diag_code,
                                'section_fk' : int(x)
                              }
    diag_json_dict['model'] = "icd10.diagnosis"
    diag_json_dict['pk']    = DIAG_ID

    json_list.append(diag_json_dict)

    yaml.dump(diag_yaml_dict, chapter_yaml, default_flow_style=False,canonical=False, indent=4)
    yaml.dump(diag_yaml_dict, diag_yaml, default_flow_style=False,canonical=False, indent=4)
    
    #db_cursor.execute("""INSERT INTO diagnosis VALUES("%s","%s", "%s");""" %(diag_name, diag_code, section_table_fk) )
    #db_conn.commit()
    DIAG_ID += 1
  print "Added Diagnosis Number: ", DIAG_ID
  return True

def _return_main_section_desc(chapter_obj):

  chapter_fk      = str(chapter_obj['chapter_id'])
  inner_sections  = chapter_obj['section_obj']
  global SECTION_ID
  for section in inner_sections:
    sectionId = section.get('id')
    diag_elem = section.iter('diag')

    #diag_elem = section.findall('diag')
    #working_diag_list = list(diag_elem)
    #print "Num of Working Diag list is: " , len(working_diag_list)
    #for elem in working_diag_list:
      #nested_diag = elem.iter('diag')
      ##print "Number of Nested Diag list is: " , len(nested_diag)
      #for el in nested_diag:
        #diag_elem.append(el)
    #print "Final Diagnosis element count is", len(diag_elem)

    table_id  = 'section_' + sectionId.replace('-','_')
    sec_id    = section.get('id').replace("-","_")
    diag_id   = section.get('id')
    desc      = section.find('desc').text

    section_yaml_dict={}
    section_yaml_dict['fields']={'sec_id'     : sec_id,
                                  'diag_id'    : diag_id,
                                  'desc'       : desc,
                                  'chapter_fk' : int(chapter_fk)
                                }
    section_yaml_dict['model'] = "icd10.section"
    section_yaml_dict['pk']    = SECTION_ID
    #yaml.dump(section_yaml_dict, chapter_yaml)

    section_json_dict={}
    section_json_dict['fields']={'sec_id'     : sec_id,
                                  'diag_id'    : diag_id,
                                  'desc'       : desc,
                                  'chapter_fk' : int(chapter_fk)
                                }
    section_json_dict['model'] = "icd10.section"
    section_json_dict['pk']    = SECTION_ID
    
    json_list.append(section_json_dict)
    
    yaml.dump(section_yaml_dict, chapter_yaml, default_flow_style=False,canonical=False, indent=4)
    yaml.dump(section_yaml_dict, section_yaml,  default_flow_style=False,canonical=False, indent=4)
    
    #main_section_desc_list.append(desc)
    print "Adding ", desc , " to Table Now.."

    #db_cursor.execute("""INSERT INTO section VALUES( "%s","%s","%s","%s");""" %(sec_id, diag_id, desc, chapter_fk))
    #db_conn.commit()

    #immediate_diag_elements.append({sec_id:diag_elem})
    _create_diag_table(diag_elem, SECTION_ID)
    
    SECTION_ID +=1
    
  #print "Returning Sections Description List: ", main_section_desc_list
  return True


def get_chapters():

  i=1
  chapter_numbers = []
  chapters = root.iter('chapter')
  print chapters

  def _get_included(elem, parentTag, innerTag):
    parentTag  = elem.find(parentTag)
    note = r''
    if parentTag:
      #print "Parent Element is: ", parentTag
      children = parentTag.findall(innerTag)
      #print children
      if children:
        for child in children:
          #print "Child Element is: ", child
          #print "Child's Text is: ", child.text
          note += child.text
          note += "\n"
        #print "Returning: ", note
        return note
      else:
        note = r"No Notes"
        return note
    else:
      return r"No Notes"

  for chapter in chapters:
    #print "Chapter if of type: ", type(chapter)
    name               = chapter.find('name').text
    desc               = chapter.find('desc').text
    includes           = _get_included(chapter, 'includes','note')
    useAdditionalCode  = _get_included(chapter, 'useAdditionalCode','note')
    excludes1          = _get_included(chapter, 'excludes1','note')
    excludes2          = _get_included(chapter, 'excludes2','note')
    sectionIndex       = _get_included(chapter, 'sectionIndex','sectionRef')
    chapter_numbers.append( str(i) )
    
    chapter_yaml_dict={}
    chapter_yaml_dict['fields']={'chapter_name'    : name,
                                'chapter_desc'     : desc,
                                'includes'         : includes,
                                'useAdditionalCode': useAdditionalCode,
                                'excludes1'        : excludes1,
                                'excludes2'        : excludes2,
                                'sectionIndex'     : sectionIndex
                                }
    chapter_yaml_dict['model'] = "icd10.chapter"
    chapter_yaml_dict['pk']    = int(i)
    
    chapter_json_dict={}
    chapter_json_dict['fields']={'chapter_name'    : name,
                                'chapter_desc'     : desc,
                                'includes'         : includes,
                                'useAdditionalCode': useAdditionalCode,
                                'excludes1'        : excludes1,
                                'excludes2'        : excludes2,
                                'sectionIndex'     : sectionIndex
                                }
    chapter_json_dict['model'] = "icd10.chapter"
    chapter_json_dict['pk']    = int(i)
    
    json_list.append(chapter_json_dict)
    
    yaml.dump(chapter_yaml_dict, chapter_yaml, default_flow_style=False,canonical=False, indent=4)
    yaml.dump(chapter_yaml_dict, chapter_yaml_text, default_flow_style=False,canonical=False, indent=4)

    #db_cursor.execute("""INSERT INTO chapters
                          #VALUES( "%s", "%s", "%s",
                                  #"%s", "%s", "%s",
                                  #"%s", "%s"
                          #);
                      #""" %( str(i),
                            #name, desc,
                            #includes,
                            #useAdditionalCode,
                            #excludes1, excludes2,
                            #sectionIndex
                          #)
    #)
    #db_conn.commit()

    inner_sections = chapter.findall('section')
    print "Created Table for Chapter No: " + str(i)
    _return_main_section_desc({'chapter_id': i, 'section_obj': inner_sections})
    
    i+=1
  return True


def setup_db_tables():
  db_cursor.execute("""CREATE TABLE IF NOT EXISTS chapters
                        ( chapter_id text,
                          name text,
                          desc varchar,
                          includes varchar,
                          useAdditionalCode varchar,
                          excludes1 varchar,
                          excludes2 varchar,
                          sectionIndex varchar,
                          PRIMARY KEY (chapter_id)
                        );"""
  )
  db_cursor.execute("""CREATE TABLE IF NOT EXISTS
                         section(sec_id text,
                                 diag_id text ,
                                 desc text,
                                 chapter_fk text,
                                 PRIMARY KEY(sec_id),
                                 FOREIGN KEY (chapter_fk) REFERENCES chapters(chapter_id)
                         );
                      """
  )
  db_cursor.execute("""CREATE TABLE IF NOT EXISTS
                        diagnosis(
                            diag_name varchar(1000),
                            diag_code text,
                            section_table_fk text,
                            FOREIGN KEY (section_table_fk) REFERENCES section(sec_id)
                        );
                    """
  )
  db_conn.commit()
  return True


#setup_db_tables()
get_chapters()
dump_the_json()

#Iterate over Child attributes
def iterate_children_of_root():
  i = 0
  if iter(root):
    for child in root:
      print "Printing Children Attributes and Text now..."
      if child.tag == 'version':
        print "Version of ICD code is: ", child.text
      elif child.tag == 'introduction':
        print "Introduction: ", child.text
        dummy_file.write("Introduction: \n" + child.text +"\n")
        dummy_file.write("_"*80+"\n")
        for children in child:
          introduction_text = "Tag: " + children.tag + "\n"+ "Attribute: "+ str(children.attrib) + "\n" + "Text: "+ children.text +"\n"
          dummy_file.write(introduction_text+"\n")
          dummy_file.write("_"*80 +"\n")
          print introduction_text
        dummy_file.close()
      elif child.tag == 'chapter':
        v=1
        for children in child:
          print "Chapter No: ", i, children.tag
          v += 1
      else:
        print "Tag: ", child.tag, "Attribute: ", child.attrib, "Text: ", child.text
      i += 1
    return True
  else:
    return None

